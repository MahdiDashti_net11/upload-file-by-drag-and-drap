(function () {
    let dropZone = document.querySelector('#drop-zone')
    let startUpload = function (files) {
        console.log(files)
    }

    document.querySelector('#standard-upload').addEventListener('click', function (e) {
        let startUploadFiles=document.querySelector('#standar-upload-files').files
        e.preventDefault()
        startUpload(startUploadFiles)
    })


    dropZone.ondrop = function (e) {
        e.preventDefault()
        this.className = 'upload-console-drop'
        startUpload(e.dataTransfer.files)
    }

    dropZone.ondragover = function () {
        this.className = 'upload-console-drop drop'
        return false
    }

    dropZone.ondragleave = function () {
        this.className = 'upload-console-drop'
        return false
    }
}())















